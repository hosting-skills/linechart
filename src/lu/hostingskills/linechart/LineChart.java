/**
 * @package     lu.hosting-skills.linechart
 * @author      Hosting Skills A.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

package lu.hostingskills.linechart;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.FilteredImageSource;

public class LineChart extends Applet {
    private Image     imgChart;

    private boolean   blnChart    = false;
    private boolean   blnLegend   = true;
    private boolean   blnMarkers  = true;

    private Color     colChart    = Color.white;
    private Color     colLegend   = Color.white;
    private Color     colPlot     = Color.lightGray;

    private Font      fntTitle    = new Font("Arial", Font.BOLD, 16);
    private Color     colTitle    = Color.black;

    private Font      fntAxis     = new Font("Arial", Font.BOLD, 13);
    private Color     colAxis     = Color.black;
    private boolean   blnAxis     = true;

    private int       intSeries   = 0;
    private Font      fntSeries   = new Font("Arial", Font.PLAIN, 13);
    private Color     colSeries   = Color.black;

    private int       intValues   = 0;
    private Font      fntValues   = new Font("Arial", Font.PLAIN, 13);
    private Color     colValues   = Color.black;
    private Color     colBorder   = Color.gray;
    private Color     colGridline = Color.black;
    private boolean   blnValues   = false;

    public void init() {
        Graphics    graChart;
        String      strTitle;
        String[]    strAxis;
        Series[]    objSeries;
        float[][]   fltValues;
        String[]    strValues;

        int         intHeight = getSize().height;
        int         intWidth  = getSize().width;

        int         intLoop;
        int         intSeries;

        int         intX;
        int         intY;
        int         intF1;
        int         intF2;
        int         intF3;
        int         intF4;
        int         intF5;
        int         intStep;
        int         intBloc;

        float       fltMax    = 0.0F;
        float       fltMin    = 0.0F;

        Font        f         = new Font("Arial", Font.PLAIN, 13);
        FontMetrics fm;
        Graphics    g;
        Hyperlink   link;
        Image       img;

        String[]    strParameter;

        strParameter = split(getParameter("debug"), ";");
        for (intLoop = 0; intLoop < strParameter.length; intLoop++) {
            if (strParameter[intLoop].equalsIgnoreCase("copyright")) {
                System.out.println("Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.");
                System.out.println("License: GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html");
                System.out.println();
            } else if (strParameter[intLoop].equalsIgnoreCase("info")) {
                String[][] strParameterInfo = getParameterInfo();
                for (String[] strInfo : strParameterInfo) {
                    for (int intInfo = 0; intInfo < strInfo.length; intInfo++) {
                        if (intInfo > 0) {
                            System.out.print(" -- ");
                        }
                        System.out.print(strInfo[intInfo]);
                    }
                    System.out.println();
                }
                System.out.println();
            } else if (strParameter[intLoop].equalsIgnoreCase("version")) {
                System.out.println(getAppletInfo());
                System.out.println();
            }
        }

        if (getParameter("legend") != null) {
            this.blnLegend = Boolean.parseBoolean(getParameter("legend"));
        }
        if (getParameter("markers") != null) {
            this.blnMarkers = Boolean.parseBoolean(getParameter("markers"));
        }

        this.colChart    = parseColor(getParameter("chartColor"),    this.colChart);
        this.colLegend   = parseColor(getParameter("legendColor"),   this.colLegend);
        this.colPlot     = parseColor(getParameter("plotColor"),     this.colPlot);
        setBackground(this.colChart);

             strTitle    = getParameter("title");
        this.fntTitle    = parseFont( getParameter("titleFont"),     this.fntTitle);
        this.colTitle    = parseColor(getParameter("titleColor"),    this.colTitle);

             strAxis     = split(getParameter("axis"), ";");
        this.fntAxis     = parseFont( getParameter("axisFont"),      this.fntAxis);
        this.colAxis     = parseColor(getParameter("axisColor"),     this.colAxis);
        if (getParameter("axisVertical") != null) {
            this.blnAxis = Boolean.parseBoolean(getParameter("axisVertical"));
        }

        try {
            this.intSeries = Integer.parseInt(getParameter("series"));
        } catch (NumberFormatException ignored) {
        }
             objSeries   = new Series[this.intSeries];
        this.fntSeries   = parseFont( getParameter("seriesFont"),     this.fntSeries);
        this.colSeries   = parseColor(getParameter("seriesColor"),    this.colSeries);
        for (intSeries = 0; intSeries < this.intSeries; intSeries++) {
            strParameter = split(getParameter("series" + intSeries), ";");
            if (strParameter.length == 0) {
                objSeries[intSeries] = new Series("", Color.black);
            } else if (strParameter.length == 1) {
                objSeries[intSeries] = new Series(strParameter[0], Color.black);
            } else if (strParameter.length == 2) {
                objSeries[intSeries] = new Series(strParameter[0], parseColor(strParameter[1], Color.black));
            } else if (strParameter.length == 3) {
                objSeries[intSeries] = new Series(strParameter[0], parseColor(strParameter[1], Color.black), strParameter[2]);
            } else {
                objSeries[intSeries] = new Series(strParameter[0], parseColor(strParameter[1], Color.black), strParameter[2], parseColor(strParameter[3], Color.black));
            }
        }

        try {
            this.intValues = Integer.parseInt(getParameter("values"));
        } catch (NumberFormatException ignored) {
        }
        this.fntValues   = parseFont( getParameter("valuesFont"),     this.fntValues);
        this.colValues   = parseColor(getParameter("valuesColor"),    this.colValues);
        this.colBorder   = parseColor(getParameter("valuesBorder"),   this.colBorder);
        this.colGridline = parseColor(getParameter("valuesGridline"), this.colGridline);
        if (getParameter("valuesVertical") != null) {
            this.blnValues = Boolean.parseBoolean(getParameter("valuesVertical"));
        }
             fltValues   = new float[this.intValues][this.intSeries];
             strValues   = new String[this.intValues];
        for (intSeries = 0; intSeries < this.intValues; intSeries++) {
            strParameter = split(getParameter("values" + intSeries), ";");
            if (strParameter.length > 0) {
                strValues[intSeries] = strParameter[0];
            }
            for (intLoop = 0; intLoop < this.intSeries; intLoop++) {
                if (intLoop + 1 < strParameter.length) {
                    try {
                             fltValues[intSeries][intLoop] = Float.parseFloat(strParameter[(intLoop + 1)]);
                        this.blnChart = true;
                        fltMax = Math.max(fltMax, fltValues[intSeries][intLoop]);
                        fltMin = Math.min(fltMin, fltValues[intSeries][intLoop]);
                    } catch (NumberFormatException e) {
                        fltValues[intSeries][intLoop] = (0.0F / 0.0F);
                    }
                } else {
                    fltValues[intSeries][intLoop] = (0.0F / 0.0F);
                }
            }
        }

        this.imgChart = createImage(intWidth, intHeight);
             graChart = this.imgChart.getGraphics();

        graChart.setColor(Color.black);
        graChart.clearRect(0, 0, intWidth, intHeight);
        graChart.drawRect(0, 0, intWidth - 1, intHeight - 1);
        intHeight -= 2;
        intWidth  -= 2;
        graChart.translate(1, 1);

        fm         = getFontMetrics(f);
        intF1      = fm.getMaxAscent();
        intF2      = fm.getMaxDescent();
        intF3      = intF1 + intF2;
        intF4      = intF3 / 2;
        intHeight -= intF3 + 2 * intF4;
        intX       = (intWidth - fm.stringWidth("Copyright © 2003-2015 Hosting Skills A.s.b.l. (info@hosting-skills.lu)")) / 2;
        intY       = intHeight + intF4;

        setLayout(null);
        graChart.setFont(f);
        graChart.drawLine(0, intHeight, intWidth, intHeight);
        graChart.drawString("Copyright © 2003-2015 ", intX, intY + intF1);
        intX += fm.stringWidth("Copyright © 2003-2015 ") + 1;

        link = new Hyperlink("Hosting Skills A.s.b.l.", f, "http://www.hosting-skills.lu/");
        link.setLocation(intX, intY);
        add(link);
        intX += link.getSize().width - 1;

        graChart.drawString(" (", intX, intY + intF1);
        intX += fm.stringWidth(" (") + 1;

        link = new Hyperlink("info@hosting-skills.lu", f, "mailto:info@hosting-skills.lu");
        link.setLocation(intX, intY);
        add(link);
        intX += link.getSize().width - 1;

        graChart.drawString(")", intX, intY + intF1);

        if (!this.blnChart) {
            return;
        }

        if ((strTitle != null) && (strTitle.length() != 0)) {
            fm         = getFontMetrics(this.fntTitle);
            intF1      = fm.getMaxAscent();
            intF2      = fm.getMaxDescent();
            intF3      = intF1 + intF2;
            intF4      = intF3 / 2;
            intHeight -= intF3 + 2 * intF4;

            graChart.setColor(this.colTitle);
            graChart.setFont(this.fntTitle);
            graChart.drawString(strTitle, (intWidth - fm.stringWidth(strTitle)) / 2, intF4 + intF1);
            graChart.translate(0, intF3 + 2 * intF4);
        }

        if (this.blnLegend) {
            fm         = getFontMetrics(this.fntSeries);
            intF1      = fm.getMaxAscent();
            intF2      = fm.getMaxDescent();
            intF3      = intF1 + intF2;
            intF4      = intF3 / 2;
            intX       = 0;
            for (intLoop = 0; intLoop < this.intSeries; intLoop++) {
                intX = Math.max(intX, fm.stringWidth(objSeries[intLoop].getName()));
            }
            intX += 3 * intF4 + 27;
            intY  = this.intSeries * (intF3 + intF4) + intF4;

            img = createImage(intX, intY);
            g   = img.getGraphics();

            g.setColor(this.colLegend);
            g.fillRect(0, 0, intX - 1, intY - 1);

            g.setColor(Color.black);
            g.drawRect(0, 0, intX - 1, intY - 1);

            g.setFont(this.fntSeries);
            for (intLoop = 0; intLoop < this.intSeries; intLoop++) {
                g.setColor(this.colSeries);
                g.drawString(objSeries[intLoop].getName(), 2 * intF4 + 27, (intLoop + 1) * (intF3 + intF4) - intF2);
                g.setColor(objSeries[intLoop].getLine());
                g.drawLine(intF4 + 1, (intLoop + 1) * (intF3 + intF4) - intF4, intF4 + 27, (intLoop + 1) * (intF3 + intF4) - intF4);
                if (this.blnMarkers) {
                    drawMarker(g, objSeries[intLoop], intF4 + 14, (intLoop + 1) * (intF3 + intF4) - intF4);
                }
            }

            intWidth -= intX + 2 * intF4;
            graChart.drawImage(img, intWidth + intF4, (intHeight - intY) / 2, this);
        }

        if (((strAxis[0] != null) && (strAxis[0].length() != 0)) ||
            ((strAxis[1] != null) && (strAxis[1].length() != 0))) {
            fm         = getFontMetrics(this.fntAxis);
            intF1      = fm.getMaxAscent();
            intF2      = fm.getMaxDescent();
            intF3      = intF1 + intF2;
            intF4      = intF3 / 2;

            if ((strAxis[0] != null) && (strAxis[0].length() != 0)) {
                intX = 0;
                if ((strAxis[1] != null) && (strAxis[1].length() != 0)) {
                    intX = 2 * intF4;
                    if (this.blnAxis) {
                        intX += intF3;
                    } else {
                        intX += fm.stringWidth(strAxis[1]);
                    }
                }

                graChart.setColor(this.colAxis);
                graChart.setFont(this.fntAxis);
                graChart.drawString(strAxis[0], (intWidth - intX - fm.stringWidth(strAxis[0])) / 2 + intX, intHeight - intF4 - intF2);

                intHeight -= intF3 + 2 * intF4;
            }

            if ((strAxis[1] != null) && (strAxis[1].length() != 0)) {
                img = createImage(fm.stringWidth(strAxis[1]), intF3);
                g   = img.getGraphics();
                g.setColor(this.colAxis);
                g.setFont(this.fntAxis);
                g.drawString(strAxis[1], 0, intF1);

                if (this.blnAxis) {
                    img = createImage(new FilteredImageSource(img.getSource(), new RotateImageFilter()));
                }

                graChart.drawImage(img, intF4, (intHeight - img.getHeight(this)) / 2, this);

                intWidth -= img.getWidth(this) + 2 * intF4;
                graChart.translate(img.getWidth(this) + 2 * intF4, 0);
            }
        }

        fm = getFontMetrics(this.fntValues);
        intF1      = fm.getMaxAscent();
        intF2      = fm.getMaxDescent();
        intF3      = intF1 + intF2;
        intF4      = intF3 / 2;
        intF5      = 0;
        if (this.blnValues) {
            for (intLoop = 0; intLoop < this.intValues; intLoop++) {
                if ((strValues[intLoop] != null) && (strValues[intLoop].length() != 0))
                    intF5 = Math.max(intF5, fm.stringWidth(strValues[intLoop]));
            }
        } else {
            intF5 = intF3;
        }

        intHeight -= intF4;
        graChart.translate(0, intF4);

        if (fltMax < 0.0F) {
            fltMax = 0.0F;
        }
        if (fltMin > 0.0F) {
            fltMin = 0.0F;
        }
        if ((fltMax == 0.0F) && (fltMin == 0.0F)) {
            fltMax = 0.5F;
        }

        intStep    = (int) Math.ceil((fltMax - fltMin) / 10.0F);
        intBloc    = roundUp(fltMax / intStep) + roundUp(-fltMin / intStep);
        intY       = (intHeight - intF4 - (intBloc + 1) - 3 - intF4 - intF5) / intBloc + 1;
        intHeight -=              intF4 + (intBloc + 1) + 3 + intF4 + intF5  + intBloc * (intY - 1);

        intF5 = fm.stringWidth("0");
        for (intLoop = roundUp(fltMax / intStep); intLoop >= -roundUp(-fltMin / intStep); intLoop--) {
            intF5 = Math.max(intF5, fm.stringWidth(Integer.toString(intLoop * intStep)));
        }
        if ((strValues[(this.intValues - 1)] != null) && (strValues[(this.intValues - 1)].length() != 0)) {
            if (this.blnValues)
                intLoop = intF4;
            else {
                intLoop = fm.stringWidth(strValues[(this.intValues - 1)]) / 2;
            }
        } else {
            intLoop = 3;
        }
        intX      = (intWidth - intF5 - intF4 - 3 - this.intValues - intLoop) / (this.intValues - 1) + 1;
        intWidth -=             intF5 + intF4 + 3 + this.intValues + intLoop  + (this.intValues - 1) * (intX - 1);

        graChart.translate(intWidth / 2, intHeight / 2);
        graChart.setFont(this.fntValues);
        graChart.setColor(this.colPlot);
        graChart.fillRect(intF5 + intF4 + 3, intF4, intX * (this.intValues - 1) + 1, intY * intBloc + 1);

        intBloc = 0;
        for (intLoop = roundUp(fltMax / intStep); intLoop >= -roundUp(-fltMin / intStep); intLoop--, intBloc++) {
            graChart.setColor(this.colValues);
            graChart.drawString(Integer.toString(intLoop * intStep), intF5 - fm.stringWidth(Integer.toString(intLoop * intStep)), intF1 + intY * intBloc);
            graChart.setColor(Color.black);
            graChart.drawLine(intF5 + intF4, intF4 + intY * intBloc, intF5 + intF4 + 2, intF4 + intY * intBloc);
            graChart.setColor(this.colGridline);
            graChart.drawLine(intF5 + intF4 + 3, intF4 + intY * intBloc, intF5 + intF4 + 3 + intX * (this.intValues - 1), intF4 + intY * intBloc);
        }
        intBloc--;
        graChart.setColor(Color.black);
        for (intLoop = 0; intLoop < this.intValues; intLoop++) {
            if ((strValues[intLoop] != null) && (strValues[intLoop].length() != 0)) {
                img = createImage(fm.stringWidth(strValues[intLoop]), intF3);
                g   = img.getGraphics();
                g.setColor(this.colValues);
                g.setFont(this.fntValues);
                g.drawString(strValues[intLoop], 0, intF1);
                if (this.blnValues) {
                    img = createImage(new FilteredImageSource(img.getSource(), new RotateImageFilter()));
                }
                graChart.drawImage(img, intF5 + intF4 + 3 + intX * intLoop - img.getWidth(this) / 2, intF4 + intY * intBloc + 5 + intF4, this);
            }
            graChart.drawLine(intF5 + intF4 + 3 + intX * intLoop, intF4 + intY * intBloc + 1, intF5 + intF4 + 3 + intX * intLoop, intF4 + intY * intBloc + 3);
        }
        intLoop--;
        graChart.translate(intF5 + intF4 + 3, intF4);
        graChart.setColor(Color.black);
        graChart.drawLine(0, 0, 0, intY * intBloc);
        graChart.drawLine(0, intY * intBloc, intX * intLoop, intY * intBloc);
        graChart.setColor(this.colBorder);
        graChart.drawRect(0, 0, intX * intLoop, intY * intBloc);

        intBloc = intY * roundUp(fltMax / intStep);
        for (intSeries = 0; intSeries < this.intSeries; intSeries++) {
            for (intLoop = 0; intLoop < this.intValues; intLoop++) {
                if (!Float.isNaN(fltValues[intLoop][intSeries])) {
                    intF5 = intBloc - Math.round(fltValues[intLoop][intSeries] / intStep * intY);
                    if ((intLoop + 1 < this.intValues) && (!Float.isNaN(fltValues[(intLoop + 1)][intSeries]))) {
                        graChart.setColor(objSeries[intSeries].getLine());
                        graChart.drawLine(intX * intLoop, intF5, intX * (intLoop + 1), intBloc - Math.round(fltValues[(intLoop + 1)][intSeries] / intStep * intY));
                    }
                    if (this.blnMarkers) {
                        drawMarker(graChart, objSeries[intSeries], intX * intLoop, intF5);
                    }
                }
            }
        }
    }

    public void paint(Graphics g) {
        g.drawImage(this.imgChart, 0, 0, this);
        super.paint(g);
    }

    private void drawMarker(Graphics g, Series s, int x, int y)
    {
        g.setColor(s.getMarker());
        if (s.isRectangle()) {
            g.fillRect(x - 3, y - 3, 7, 7);
            g.setColor(Color.black);
            g.drawRect(x - 3, y - 3, 6, 6);
        } else {
            int[] xPoints;
            int[] yPoints;
            if (s.isDiamond()) {
                xPoints = new int[4];
                yPoints = new int[4];
                xPoints[0] = x - 3;
                xPoints[1] = x;
                xPoints[2] = x + 3;
                xPoints[3] = x;
                yPoints[0] = y;
                yPoints[1] = y - 3;
                yPoints[2] = y;
                yPoints[3] = y + 3;
                g.fillPolygon(xPoints, yPoints, 4);
                g.setColor(Color.black);
                g.drawPolygon(xPoints, yPoints, 4);
            } else if (s.isTriangle()) {
                xPoints = new int[3];
                yPoints = new int[3];
                xPoints[0] = x - 3;
                xPoints[1] = x;
                xPoints[2] = x + 3;
                yPoints[0] = y + 3;
                yPoints[1] = y - 3;
                yPoints[2] = y + 3;
                g.fillPolygon(xPoints, yPoints, 3);
                g.setColor(Color.black);
                g.drawPolygon(xPoints, yPoints, 3);
            } else if (s.isCircle()) {
                g.fillOval(x - 3, y - 3, 7, 7);
                g.setColor(Color.black);
                g.drawOval(x - 3, y - 3, 6, 6);
            }
        }
    }

    private Color parseColor(String strColor, Color colColor) {
        try {
            return new Color(Integer.decode(strColor));
        } catch (NumberFormatException ignored) {
        } catch (NullPointerException ignored) {
        }
        return colColor;
    }

    private Font parseFont(String strFont, Font fntFont)
    {
        String strName  = fntFont.getName();
        int    intStyle = fntFont.getStyle();
        int    intSize  = fntFont.getSize();

        String[] strParameter = split(strFont, ";");
        if (strParameter.length > 0 && strParameter[0].length() > 0) {
            strName = strParameter[0];
        }
        if (strParameter.length > 1) {
            if (strParameter[1].equalsIgnoreCase("PLAIN")) {
                intStyle = Font.PLAIN;
            } else if (strParameter[1].equalsIgnoreCase("BOLD")) {
                intStyle = Font.BOLD;
            } else if (strParameter[1].equalsIgnoreCase("ITALIC")) {
                intStyle = Font.ITALIC;
            } else if (strParameter[1].equalsIgnoreCase("BOLD|ITALIC")) {
                intStyle = Font.BOLD | Font.ITALIC;
            }
        }
        if (strParameter.length > 2) {
            try {
                intSize = Integer.parseInt(strParameter[2]);
            } catch (NumberFormatException ignored) {
            }
        }

        return new Font(strName, intStyle, intSize);
    }

    public int roundUp(float fltNumber)
    {
        float fltCeil = (float) Math.ceil(fltNumber);

        if (Float.isNaN(fltNumber) || Float.isInfinite(fltNumber) || fltNumber == 0.0F) {
            fltCeil = fltNumber;
        } else if (fltCeil == fltNumber) {
            fltCeil++;
        }
        return (int) fltCeil;
    }

    public String[] split(String str, String regex)
    {
        String[] arr = {};

        if (str == null) {
            return arr;
        }

        if (regex == null) {
            arr = new String[0];
            arr[0] = str;
            return arr;
        }

        return str.split(regex);
    }

    public String getAppletInfo() {
        return "LineChart ver. 1.1.0 by Hosting Skills A.s.b.l. (info@hosting-skills.lu)";
    }

    public String[][] getParameterInfo() {
        return new String[][] {
            { "debug",          "delimited string", "A semi-colon-delimited list defining the debug options. It takes the form of <option#1>;<option#2>;... where <option#> is one of \"copyright\", \"info\" or \"version\". Default is \"\" (no debug options)." },
            { "legend",         "boolean",          "Show legend? Default is true." },
            { "markers",        "boolean",          "Display markers? Default is true." },
            { "chartColor",     "#rrggbb",          "The color of the chart area. Default is #ffffff (white)." },
            { "legendColor",    "#rrggbb",          "The color of the legend area. Default is #ffffff (white)." },
            { "plotColor",      "#rrggbb",          "The color of the plot area. Default is #c0c0c0 (lightGray)." },
            { "title",          "string",           "The chart title of the line chart. Default is \"\" (no title)." },
            { "titleFont",      "delimited string", "A semi-colon-delimited list defining the font of the chart title. It takes the form of <name>;<style>;<size> where <name> is a string, <style> is one of \"PLAIN\", \"BOLD\", \"ITALIC\" or \"BOLD|ITALIC\" and <size> is an int. Default is Arial;Bold;16." },
            { "titleColor",     "#rrggbb",          "The color of the chart title. Default is #000000 (black)." },
            { "axis",           "delimited string", "A semi-colon-delimited list defining the axis titles. It takes the form of <category (x) axis title>;<value (y) axis title>. Default is \"\" (no axis titles)." },
            { "axisFont",       "delimited string", "A semi-colon-delimited list defining the font of the axis titles. It takes the form of <name>;<style>;<size> where <name> is a string, <style> is one of \"PLAIN\", \"BOLD\", \"ITALIC\" or \"BOLD|ITALIC\" and <size> is an int. Default is Arial;Bold;13." },
            { "axisColor",      "#rrggbb",          "The color of the axis titles. Default is #000000 (black)." },
            { "axisVertical",   "boolean",          "The orientation of the value (y) axis title. Default is true (vertical)." },
            { "series",         "int",              "The number of data series. Default is 0 (no series and no line chart)." },
            { "seriesFont",     "delimited string", "A semi-colon-delimited list defining the font of the legend. It takes the form of <name>;<style>;<size> where <name> is a string, <style> is one of \"PLAIN\", \"BOLD\", \"ITALIC\" or \"BOLD|ITALIC\" and <size> is an int. Default is Arial;Regular;13." },
            { "seriesColor",    "#rrggbb",          "The color of the legend. Default is #000000 (black)." },
            { "series<#m>",     "delimited string", "A semi-colon-delimited list defining the series #m data. It takes the form of <name>;<line color>;<marker type>;<marker color> where <name> is a string, <line color> is a #rrggbb color, <marker type> is one of \"RECTANGLE\", \"DIAMOND\", \"TRIANGLE\" or \"CIRCLE\" and <marker color> is a #rrggbb color. Default is \"\";#000000;RECTANGLE;<line color>." },
            { "values",         "int",              "The number of values in a data series. Default is 0 (no values and no line chart)." },
            { "valuesFont",     "delimited string", "A semi-colon-delimited list defining the font of the category (x) axis labels. It takes the form of <name>;<style>;<size> where <name> is a string, <style> is one of \"PLAIN\", \"BOLD\", \"ITALIC\" or \"BOLD|ITALIC\" and <size> is an int. Default is Arial;Regular;13." },
            { "valuesBorder",   "#rrggbb",          "The color of the plot area border. Default is #808080 (gray)." },
            { "valuesColor",    "#rrggbb",          "The color of the category (x) axis labels. Default is #000000 (black)." },
            { "valuesGridline", "#rrggbb",          "The color of the gridlines. Default is #000000 (black)." },
            { "valuesVertical", "boolean",          "The orientation of the category (x) axis labels. Default is false (horizontal)." },
            { "values<#n>",     "delimited string", "A semi-colon-delimited list defining the #n values of the series. It takes the form <category (x) axis label>;<value#1>;<value#2>;...;<value#m> where <category (x) axis label> is a string and <value#> is a float. Default is \"\";NaN;NaN;...;NaN" }
        };
    }
}