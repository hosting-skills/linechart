/**
 * @package     lu.hosting-skills.linechart
 * @author      Hosting Skills A.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

package lu.hostingskills.linechart;

import java.awt.Color;

public class Series {
    private static final int RECTANGLE = 0;
    private static final int DIAMOND = 1;
    private static final int TRIANGLE = 2;
    private static final int CIRCLE = 3;

    private String strName;
    private Color  colLine;
    private Color  colMarker;
    private int    intMarker;

    public Series(String strName, Color colColor) {
        this(strName, colColor, "RECTANGLE", colColor);
    }

    public Series(String strName, Color colColor, String strMarker) {
        this(strName, colColor, strMarker, colColor);
    }

    public Series(String strName, Color colLine, String strMarker, Color colMarker) {
        this.strName = strName;
        this.colLine = colLine;
        if (strMarker.equalsIgnoreCase("RECTANGLE")) {
            this.intMarker = Series.RECTANGLE;
        } else if (strMarker.equalsIgnoreCase("DIAMOND")) {
            this.intMarker = Series.DIAMOND;
        } else if (strMarker.equalsIgnoreCase("TRIANGLE")) {
            this.intMarker = Series.TRIANGLE;
        } else if (strMarker.equalsIgnoreCase("CIRCLE")) {
            this.intMarker = Series.CIRCLE;
        } else {
            this.intMarker = Series.RECTANGLE;
        }
        this.colMarker = colMarker;
    }

    public String getName() {
        return this.strName;
    }

    public Color getLine() {
        return this.colLine;
    }

    public Color getMarker() {
        return this.colMarker;
    }

    public boolean isRectangle() {
        return this.intMarker == Series.RECTANGLE;
    }

    public boolean isDiamond() {
        return this.intMarker == Series.DIAMOND;
    }

    public boolean isTriangle() {
        return this.intMarker == Series.TRIANGLE;
    }

    public boolean isCircle() {
        return this.intMarker == Series.CIRCLE;
    }

    public String toString() {
        String str = "";

        if (isRectangle()) {
            str = "RECTANGLE";
        } else if (isDiamond()) {
            str = "DIAMOND";
        } else if (isTriangle()) {
            str="TRIANGLE";
        } else if (isCircle()) {
            str = "CIRCLE";
        }

        return "Name: " + this.strName + "; Line-Color: " + this.colLine.toString() + "; Marker-Color: " + this.colMarker.toString() + "; Marker-Type: " + str;
    }
}