/**
 * @package     lu.hosting-skills.linechart
 * @author      Hosting Skills A.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

package lu.hostingskills.linechart;

import java.applet.Applet;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HyperlinkListener extends MouseAdapter {
    public void mousePressed(MouseEvent e) {
        if (e.getComponent().getClass().getName().equalsIgnoreCase("lu.hostingskills.linechart.Hyperlink")) {
            ((Applet) e.getComponent().getParent()).getAppletContext().showDocument(((Hyperlink) e.getComponent()).getURL(), "_blank");
        }
    }

    public void mouseEntered(MouseEvent e) {
        if (e.getComponent().getClass().getName().equalsIgnoreCase("lu.hostingskills.linechart.Hyperlink")) {
            ((Hyperlink) e.getComponent()).setFocus(true);
        }
    }

    public void mouseExited(MouseEvent e) {
        if (e.getComponent().getClass().getName().equalsIgnoreCase("lu.hostingskills.linechart.Hyperlink")) {
            ((Hyperlink) e.getComponent()).setFocus(false);
        }
    }
}