/**
 * @package     lu.hosting-skills.linechart
 * @author      Hosting Skills A.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

package lu.hostingskills.linechart;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.net.MalformedURLException;
import java.net.URL;

public class Hyperlink extends Component {
    private boolean blnFocus = false;
    private String  strText;
    private URL     urlURL;

    public Hyperlink(String strText, Font f, String strURL) {
        FontMetrics fm = getFontMetrics(f);

        this.strText = strText;
        try {
            this.urlURL = new URL(strURL);
        }
        catch (MalformedURLException ignore) {
        }

        addMouseListener(new HyperlinkListener());
        setFont(f);
        setSize(fm.stringWidth(strText), fm.getMaxAscent() + fm.getMaxDescent() + 2);
    }

    public void paint(Graphics g) {
        g.setFont(getFont());
        if (this.blnFocus) {
            g.setColor(Color.red);
            g.drawLine(0, getSize().height - 1, getSize().width - 1, getSize().height - 1);
        } else {
            g.setColor(Color.blue);
        }
        g.drawString(this.strText, 0, getFontMetrics(getFont()).getMaxAscent() + 1);
    }

    public URL getURL() {
        return this.urlURL;
    }

    public void setFocus(boolean blnFocus) {
        this.blnFocus = blnFocus;
        repaint();
    }

    public String toString() {
        return "Text: " + this.strText + "; URL: " + this.urlURL.toString();
    }
}