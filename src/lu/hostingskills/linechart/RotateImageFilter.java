/**
 * @package     lu.hosting-skills.linechart
 * @author      Hosting Skills A.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2003-2016 Hosting Skills A.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

package lu.hostingskills.linechart;

import java.awt.image.ColorModel;
import java.awt.image.ImageFilter;

public class RotateImageFilter extends ImageFilter {
    public void setPixels(int x, int y, int w, int h, ColorModel model, byte[] pixelsOld, int off, int scansize) {
        byte[] pixelsNew = new byte[pixelsOld.length];

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                pixelsNew[(i * h + j)] = pixelsOld[(j * scansize + (scansize - i - 1) + off)];
            }
        }

        this.consumer.setPixels(y, x, h, w, model, pixelsNew, 0, h);
    }

    public void setPixels(int x, int y, int w, int h, ColorModel model, int[] pixelsOld, int off, int scansize) {
        int[] pixelsNew = new int[pixelsOld.length];

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                pixelsNew[(i * h + j)] = pixelsOld[(j * scansize + (scansize - i - 1) + off)];
            }
        }

        this.consumer.setPixels(y, x, h, w, model, pixelsNew, 0, h);
    }
}
